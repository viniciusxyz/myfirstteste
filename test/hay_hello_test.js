import { equal } from 'assert'
import sayHello from '../src/say_hello'

describe('sayHello test',() => {
    it('should return greet with excitment',() => {
        equal(sayHello('ue'), 'Hello ue!');
    })
})