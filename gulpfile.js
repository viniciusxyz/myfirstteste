var browserify = require('browserify');
var babel = require('babelify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');

gulp.task('browserify',function(){
    return browserify('./src/app.js')
        .transform(babelify.configure({
            presets: ["@babel/preset-env"]
        }))
        .bundle()
        .pipe(source('app.js'))
        .pipe(gulp.dest('./public/'));
});


